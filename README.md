# awesome_dotfiles

My dotfiles (configuration) for an awesomewm desktop environment. The README also contains which programs I used (and/or are needed)

# Programs used
- rofi (run prompt, awesome prompts are just plain ugly and rofi lists matches)
- picom (compositor, forked from compton as compton is discontinued)
- urxvt (Terminal emulator, will probably try out a better one)
- kate (Text editor)

# NOTE
The theme is currently WIP, I made rounded corners (which awesome can't anti-alias 
sadly) and the textures for close, maximize and minimize but I plan to add more
stuff on the go (like a settings menu for example).

# TODO:
- [ ] Get a better colorscheme
- [ ] Change terminal emulator to something better (like Kitty)
- [ ] Clean up directory and file naming
- [ ] Add hover and press textures for titlebar buttons
- [ ] (?) Create something like a sidebar-menu where you can configure the whole setup
    - [ ] Create a "settings.lua" file storing your settings